public class Sheep extends Herbivore implements Thirsty{
    private boolean thirsty = true;

    @Override
    public void drink() {
        this.thirsty = false;
    }

    @Override
    public void toilet() {
        this.thirsty = true;
    }

    @Override
    public void report() {
        if(thirsty)
            System.out.println("Хочу пить");
        else
            System.out.println("Хочу в туалет");
    }
    public Sheep(String name){
        super(name);
        this.sizeAviary = SizeAviary.MEDIUM;
    }
    @Override
    public void voice() {
        System.out.println("Беее");
    }
}
