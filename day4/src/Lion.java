public class Lion extends Carnivorous implements Play{
    @Override
    public void play() {
        System.out.println("Какой дурак играет со львом?..");
    }
    public Lion(String name){
        super(name);
        this.sizeAviary = SizeAviary.LARGE;
    }
    @Override
    public void voice() {
        System.out.println("Ррррррррооооаааааарррр");
    }
}
