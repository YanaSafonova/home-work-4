public abstract class Herbivore extends Animal{
    public Herbivore(String name){
        super(name);
    }
    @Override
    public void eat(Food food) throws WrongFoodExeption {
        if(food instanceof Grass) {
            this.hungry = false;
        }
        else
            throw new WrongFoodExeption();
    }
}
