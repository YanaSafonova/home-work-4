public abstract class Carnivorous extends Animal{
    public Carnivorous(String name){
        super(name);
    }
    @Override
    public void eat(Food food) throws WrongFoodExeption {
        if(food instanceof Meat) {
            this.hungry = false;
        }
        else
            throw new WrongFoodExeption();

    }
}
