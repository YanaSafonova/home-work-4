import java.util.ArrayList;

public class Zoo {
    public static int sizeCarn = 3;
    public static int sizeHerb = 5;
    public Aviary<Carnivorous> aviaryCarn ;
    public Aviary<Herbivore> aviaryHerb;
    public Zoo(){

        this.aviaryCarn = new Aviary(sizeCarn,SizeAviary.MEDIUM);
        this.aviaryHerb = new Aviary(sizeHerb,SizeAviary.MEDIUM);
    }
    public static void main(String[] args) throws WrongFoodExeption {
        Zoo zoo = new Zoo();
        Meat meat = new Meat();
        Grass grass = new Grass();
        Lion lion = new Lion("Симба");
        Eagle eagle = new Eagle("Зазу");
        Tiger tiger = new Tiger("Шерхан");
        Duck duck = new Duck("Дональд");
        Lemur lemur = new Lemur("Джулиан");
        Sheep sheep = new Sheep("Шон");


       // lion.eat(grass); //выбрасывает исключение
        lion.eat(meat);
        lion.getHungry();


        zoo.aviaryCarn.addAnimal(lion);//Вольер мал для льва
        zoo.aviaryCarn.addAnimal(tiger);//Вольер мал для тигра
        zoo.aviaryCarn.addAnimal(eagle);


      zoo.aviaryHerb.addAnimal(duck);
      zoo.aviaryHerb.addAnimal(sheep);
      zoo.aviaryHerb.addAnimal(lemur);


    }
}
