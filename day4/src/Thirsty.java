public interface Thirsty {
    public void drink();
    public void toilet();
    public void report();
}
