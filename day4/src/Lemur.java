public class Lemur extends Herbivore implements Play{
    @Override
    public void play() {
        System.out.println("С тобой так весело играть!");
    }

    public Lemur(String name){
        super(name);
        this.sizeAviary = SizeAviary.TINY;
    }
    @Override
    public void voice() {
        System.out.println("Кричит от русской погоды");
    }
}
